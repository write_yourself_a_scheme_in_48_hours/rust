use std::env;

fn main() {
    match &env::args().collect::<Vec<_>>()[..] {
        [a0] => println!("{}", a0),
        [_, a1] => println!("{}", a1),
        _ => println!("{:?}", env::args().collect::<Vec<_>>()),
    }
}
